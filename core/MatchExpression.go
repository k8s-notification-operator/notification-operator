/*
 *    Copyright 2019 Lucas Ces Santos
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package core

import (
	"github.com/Jeffail/gabs"
	"strings"
)

type MatchExpression struct {
	Key      string   `json:"key"`
	Operator string   `json:"operator"`
	Values   []string `json:"values"`
}

func (m MatchExpression) Evaluate(data []byte) bool {
	jsonParsed, err := gabs.ParseJSON(data)
	if err != nil {
		return false
	}
	field, found := jsonParsed.Path(m.Key).Data().(string)
	if !found {
		println("Field Not Found")
		return false
	}

	r := false
	switch strings.ToLower(m.Operator) {
	case "in":
		r = m.evaluateIn(field)
	case "notin":
		r = false
	case "exists":
		r = true
	case "doesnotexist":
		r = false
	case "gt":
	case "lt":
		r = false
	default:
		r = false
	}
	return r
}

func (m MatchExpression) evaluateIn(field string) bool {
	return contains(m.Values, field)
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
